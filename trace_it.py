#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pyqbdi
import tempfile
import os

x64_reg_list = ["rax", "rbx", "rcx", "rdx", "rsp", "rbp", "rsi", "rdi", "rip", "r8", "r9", "r10", "r11", "r12", "r13", "r14", "r15"]
x32_reg_list = ["eax", "ebx", "ecx", "edx", "esp", "ebp", "esi", "edi", "eip"]

bb_reg_states = []

fd, path = tempfile.mkstemp()

# If the binary crashes, we never flush our reg states
# so we write them everytime
def update_output(reg_state):
    os.write(fd, str(reg_state).encode())
    os.fsync(fd)

def mycb(vm, event, gpr, fpr, data):

    reg_state = vm.getGPRState()

    update_output(reg_state)

    #bb_reg_states.append(reg_state)

    return pyqbdi.CONTINUE


def pyqbdipreload_on_run(vm, start, stop):
    print(path, flush=True)
    vm.addVMEventCB(pyqbdi.BASIC_BLOCK_EXIT, mycb, None)
    vm.run(start, stop)