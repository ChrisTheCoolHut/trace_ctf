from celery import Celery

app = Celery('CeleryTask')
app.config_from_object('celeryconfig')
#python -m pyqbdipreload ./trace_it.py /home/chris/ctf/metactf2020/rev_2/rop_easy
qbdi_command = "python -m pyqbdipreload /home/chris/projects/trace_ctf/trace_it.py {} {}"
# qemu-x86_64 -D LOG_FILE_PATH -d cpu BINARY_NAME ARG
qemu_command = "qemu-x86_64 -D {} -d cpu,nochain {} {}"
import subprocess
import shlex
import tempfile, shutil, os

@app.task
def run_qbdi_command(content, binary, character, stdin_input=True):
    ins_count = -1
    if stdin_input:

        content = str.encode(content)
        proc_command = qbdi_command.format(
                binary, "")

        qbdi_proc = subprocess.Popen(proc_command,
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE,
                shell=True)

        reg_file = qbdi_proc.communicate(content)[0].splitlines()[0]
        
    else:
        proc_command = qbdi_command.format(
                binary, content)

        qbdi_proc = subprocess.Popen(proc_command, shell=True,
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE)

        reg_file = qbdi_proc.communicate()[0].splitlines()[0]

    reg_file = reg_file.decode()
    print(content)
    print(reg_file)
    return (character, reg_file)

@app.task
def run_qemu_command(content, binary, character, stdin_input=True):
    fd, path = tempfile.mkstemp()

    if stdin_input:

        content = str.encode(content)
        proc_command = qemu_command.format(
                path, binary, "")

        qbdi_proc = subprocess.Popen(proc_command,
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE,
                shell=True)

        reg_file = qbdi_proc.communicate(content)[0].splitlines()[0]
        
    else:
        proc_command = qemu_command.format(
                path, binary, content)

        qbdi_proc = subprocess.Popen(proc_command, shell=True,
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE)

        reg_file = qbdi_proc.communicate()[0].splitlines()[0]

    reg_file = reg_file.decode()
    print(content)
    print(reg_file)
    return (character, path)