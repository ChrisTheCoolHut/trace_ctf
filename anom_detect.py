import sklearn
import pickle
import operator
import re
from tqdm import tqdm
import numpy as np
import scipy.stats as stats
from scipy.stats import uniform
from scipy.stats import norm

x64_reg_list = ["rax", "rbx", "rcx", "rdx", "rsp", "rbp", "rsi", "rdi", "rip", "r8", "r9", "r10", "r11", "r12", "r13", "r14", "r15"]
x32_reg_list = ["eax", "ebx", "ecx", "edx", "esp", "ebp", "esi", "edi", "eip"]

VERBOSE = False


def get_reg_states_from_file(file_path):

    reg_state_list = []
    with open(file_path, "r") as f:

        reg_state = {}
        for line in f:
            if "GPRState begin" in line:
                reg_state = {}
            elif "GPRState end" in line:
                reg_state_list.append(reg_state)
            else:
                line_split = line.split(":")
                reg = line_split[0].rstrip()
                val = int(line_split[1].rstrip(), 16)
                reg_state[reg] = val

    return reg_state_list

# echo 0 | sudo tee /proc/sys/kernel/randomize_va_space
def get_qemu_reg_states_from_file(file_path):

    reg_state_list = []
    with open(file_path, "r") as f:

        reg_state = {}
        for line in f:
            if "RAX=" in line:
                # New state
                reg_state = {}
                regs = re.findall("[A-Z| |0-9]{3}=[a-f|0-9]{16}", line)
                for reg in regs:
                    reg_split = reg.split('=')
                    reg_state[reg_split[0].rstrip().lower()] = int(reg_split[1].rstrip(),16)

            elif "RIP=" in line:
                # End of state
                regs = re.findall("[A-Z| |0-9]{3}=[a-f|0-9]{16}", line)
                for reg in regs:
                    reg_split = reg.split('=')
                    reg_state[reg_split[0].rstrip().lower()] = int(reg_split[1].rstrip(),16)
                reg_state_list.append(reg_state)
                reg_state = {}
            elif "RSI=" in line or "R8" in line or "R12" in line:
                regs = re.findall("[A-Z| |0-9]{3}=[a-f|0-9]{16}", line)
                for reg in regs:
                    reg_split = reg.split('=')

    return reg_state_list

def drop_outside_std_dev(reg_state_list, deviations=1):
    basic_block_counts = {}

    for x, y in reg_state_list.items():
        basic_block_counts[x] = len(y)

    unique_counts = set(basic_block_counts.values())
    counts = np.array(list(basic_block_counts.values()))
    std_dev = np.std(counts)
    counts_mean = np.mean(counts)
    for x,y in basic_block_counts.items():
        # if 1 deviation outside of list
        if y < counts_mean - (std_dev * deviations) or y > counts_mean + (std_dev * deviations):
            print("Dropping character {} outisde of std_dev with len {}".format(repr(x),y))
            del reg_state_list[x]

def basic_blocks_are_equal(bb_reg_states):

    if all(bb_reg_states[0] == x for x in bb_reg_states):
        # print("Basic block {} is equal".format(bb_reg_states[0]['rip']))
        return True

def pc_is_equal(bb_reg_states):

    if all(bb_reg_states[0]['rip'] == x['rip'] for x in bb_reg_states):
        return True

def who_is_sus(basic_block_reg_list, results):

    rax_list = [x['rax'] for x in basic_block_reg_list]

    my_list = [(x,y,z) for x,y,z in zip(dict(results).keys(), rax_list, stats.zscore(rax_list))]
    # Sort by z score
    my_list.sort(key=lambda x: x[2])

    return my_list

# echo 0 | sudo tee /proc/sys/kernel/randomize_va_space

def get_anom_edges(results):

    reg_state_list = {}

    for x, y in tqdm(results, total=len(results)):
        #reg_state_list[x] = get_reg_states_from_file(y)
        reg_state_list[x] = get_qemu_reg_states_from_file(y)

    drop_outside_std_dev(reg_state_list)

    drop_outside_std_dev(reg_state_list, deviations=2)

    basic_block_counts = {}

    for x, y in reg_state_list.items():
        basic_block_counts[x] = len(y)

    max_basic_block_count = max(basic_block_counts.items(), key=operator.itemgetter(1))[1]

    ret_dict = {}

    for basic_block_iter in range(max_basic_block_count):
        basic_block_reg_list = []
        basic_block_reg_list_letter = []
        for x, y in reg_state_list.items():
            if basic_block_iter < len(reg_state_list[x]):
                reg_state = reg_state_list[x][basic_block_iter]
                basic_block_reg_list.append(reg_state)
                basic_block_reg_list_letter.append(x)
        if not basic_blocks_are_equal(basic_block_reg_list):
            if pc_is_equal(basic_block_reg_list):

                sus_list = who_is_sus(basic_block_reg_list, results)
                rax_list = [x['rax'] for x in basic_block_reg_list]

                # Only want a single match
                if [x['rax'] for x in basic_block_reg_list].count(0) == 1:

                    sus_list = who_is_sus(basic_block_reg_list, results)
                    rax_list = [x['rax'] for x in basic_block_reg_list]

                    if abs(sus_list[0][2]) > 1 or abs(sus_list[-1][2]) > 1:
                        rax_norm = np.linalg.norm(rax_list)

                        if VERBOSE:
                            print("Hmmm {} and {} are kinda sus".format(sus_list[0][0], sus_list[-1][0]))
                            print("Norm : {}".format(rax_norm))
                            print(sus_list[0])
                            print(sus_list[-1])

                        min_val = (sus_list[0][0],"min")
                        max_val = (sus_list[-1][0],"max")

                        # We'll be sorting and grabbing from this dict later
                        # if we get the entry again, average the norms
                        if min_val not in ret_dict.keys():
                            ret_dict[min_val] = rax_norm
                        else:
                            ret_dict[min_val] = (ret_dict[min_val] + rax_norm) / 2
                        if max_val not in ret_dict.keys():
                            ret_dict[max_val] = rax_norm
                        else:
                            ret_dict[max_val] = (ret_dict[max_val] + rax_norm) / 2

                    if VERBOSE:
                        print("=====Found important one")
                        print(basic_block_reg_list[0])
                        print("CRITICAL BLOCK DETECTED AT {}".format(hex(basic_block_reg_list[0]["rip"])))
            else:
                pass

    # Sort ret_dict in desending order by their averaged norms
    letter_list = {k: v for k, v in sorted(ret_dict.items(), key=lambda item: item[1], reverse=True)}
    letter_list = list(letter_list)
    min_letter = None
    max_letter = None
    for letter in letter_list:
        if min_letter == None and letter[1] == 'min':
            min_letter = letter[0]
        if max_letter == None and letter[1] == 'max':
            max_letter = letter[0]

    print("min is {}, max is {}".format(min_letter, max_letter))

    return min_letter, max_letter


# my_data = "first_run.pickle"

# results = pickle.load(open(my_data, "rb"))

# get_anom_edges(results)